package com.company.file;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Andrey_Vaganov on 12/5/2016.
 */
public class MainReader {

    /**
     * Формат даты
     */
    private static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";

    /**
     * Форматтер, используется для преобразования строк в даты и обратно
     */
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);

    private static final Logger LOGGER = LogManager.getLogger(MainReader.class); //logger init

    /**
     * Точка входа в программу
     *
     * @param args
     */
    public static void main(String[] args) throws ParseException {
        readFile();
    }

    /**
     * Метод для чтения дат из файла
     */
    public static void readFile() {
        String fileName = "file.txt";
        try(BufferedReader byfReader = new BufferedReader(new FileReader(fileName))) {
            FileReader reader = new FileReader(fileName);
           // BufferedReader byfReader = new BufferedReader(reader);

        //Открываем потоки на чтение из файла
        //Читаем первую строку из файла

         String strDate = byfReader.readLine();


        while (strDate != null) {
            //Преобразуем строку в дату
            Date date = parseDate(strDate);

            //Выводим дату в консоль в формате dd-mm-yy
            System.out.printf("%1$td-%1$tm-%1$ty \n", date);

            //Читаем следующую строку из файла
            strDate = byfReader.readLine();

        }
        } catch (IOException e) {
            LOGGER.error("IOException in main, try to read file.txt", e);
        }

    }

    /**
     * Метод преобразует строковое представление даты в класс Date
     *
     * @param strDate строковое представление даты
     * @return
     */
    public static Date parseDate(String strDate) {
        try {
            return dateFormatter.parse(strDate);
        } catch (ParseException e){
            LOGGER.error("something wrong with parsing strDate");
        }
        return null;
    }
}