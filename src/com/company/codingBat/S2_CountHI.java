package com.company.codingBat;

public class S2_CountHI {
    String hi = "hi";
    String  substring = "";
    int sum = 0;
    public S2_CountHI(String str) {
        for (int i = 0; i < str.length() - 1; i++) {
            substring = str.substring(i, i+2);
            if (substring.compareToIgnoreCase(hi) == 0)
                this.sum++;
        }
        System.out.println("Number of 'Hi' in string is equal to " + sum);
    }

}
