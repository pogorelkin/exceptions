package com.company.codingBat;

/**
 * We want make a package of goal kilos of chocolate. We have small bars (1 kilo each) and big bars (5 kilos each).
 * Return the number of small bars to use, assuming we always use big bars before small bars. Return -1 if it can't be done.
 * makeChocolate(4, 1, 9) → 4
 * makeChocolate(4, 1, 10) → -1
 * makeChocolate(4, 1, 7) → 2
 */

public class L2_makeChocolate {
    public int makeChocolate(int small, int big, int goal) {
        int bigWeight = 5;

        int temp = 0;
        temp = small + big * bigWeight;
        if (temp < goal) {
            return -1;
        }

        temp = goal - big * bigWeight;
        if (small >= temp) {
            return small - (small - temp);
        } else {
            return -1;
        }

    }


}




